using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardMoveBtns : MonoBehaviour
{
    [SerializeField] GameObject CardMoveBtnsObject;
    [SerializeField] GameObject SystemObject;

    private GameObject MoveCard;
    public void onLeftFieldMoveBtn()
    {
        CardMoveBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().changeFieldList(0, MoveCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void onCenterFieldMoveBtn()
    {
        CardMoveBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().changeFieldList(1, MoveCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void onRightFieldMoveBtn()
    {
        CardMoveBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().changeFieldList(2, MoveCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void onBackLeftFieldMoveBtn()
    {
        CardMoveBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().changeFieldList(3, MoveCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void onBackRightFieldMoveBtn()
    {
        CardMoveBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().changeFieldList(4, MoveCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void setMoveCard(GameObject Card)
    {
        MoveCard = Card;
    }
}
