using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InquiryDialog : EnumManager
{
    [SerializeField] Text text;
    [SerializeField] GameObject System;
    [SerializeField] GameObject FieldCardSetBtns;
    [SerializeField] GameObject PlayCard;

    public void setPlayCard(GameObject card)
    {
        this.PlayCard = card;
    }

    public void setText(string str)
    {
        text.GetComponent<Text>().text = str;
    }

    public void onNoBtn()
    {
        this.gameObject.SetActive(false);
        System.GetComponent<Game>().FixPosition();
    }

    public void onYesBtn()
    {
        this.gameObject.SetActive(false);

        // Characterのとき
        if(PlayCard.GetComponent<Card>().GetType() == (int)Type.Character)
        {
            FieldCardSetBtns.SetActive(true);
            FieldCardSetBtns.GetComponent<FieldCardSetBtns>().setSetCard(PlayCard);
        }
        // Climaxのとき
        else if (PlayCard.GetComponent<Card>().GetType() == (int)Type.Climax)
        {
            PlayCard.GetComponent<Card>().changeNowZone((int)Zone.CLIMAX);
            System.GetComponent<Game>().removeHandList(PlayCard);
            System.GetComponent<Game>().setClimax(PlayCard);
            System.GetComponent<Game>().FixPosition();
            System.GetComponent<TimeManager>().nextPhase();
        }
        // Eventのとき
        else if (PlayCard.GetComponent<Card>().GetType() == (int)Type.Event)
        {
            System.GetComponent<EventManager>().onEventManager(PlayCard.GetComponent<Card>().cardNo);
        }
    }
}
