﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : EnumManager
{
    [SerializeField] GameObject cardImage;
    [SerializeField] GameObject System;
    [SerializeField] GameObject CardPower;

    public int level;
    public int cost;
    public string trigger;
    public int nowPower;
    public int originalPower;
    public string cardNo;
    public string name;
    public int soul;
    public List<string> character;
    public string color;
    [SerializeField] Type type;
    public bool haveActEffect;
    public Sprite spriteImage;

    private Zone nowZone;
    private Status status;

    // Update is called once per frame
    void Update()
    {
        if (nowZone == Zone.FIELD)
        {
            changeCardPower(nowPower);
        }
    }

    public void spriteChange()
    {
        cardImage.GetComponent<Image>().sprite = spriteImage;
    }

    public void switchPowerDisplay(bool switchBool)
    {
        CardPower.SetActive(switchBool);
    }

    public void changeCardPower(int cardPower)
    {
        CardPower.GetComponent<Text>().text = cardPower.ToString();
    }

    public void changeNowZone(int zone)
    {
        nowZone = (Zone)Enum.ToObject(typeof(Zone), zone);
    }

    public int GetNowZone()
    {
        return (int)nowZone;
    }

    public void changeNowStatus(int num)
    {
        status = (Status)Enum.ToObject(typeof(Status), num);
    }

    public int GetNowStatus()
    {
        return (int)status;
    }

    public void SetType(int num)
    {
        type = (Type)Enum.ToObject(typeof(Type), num);
    }

    public int GetType()
    {
        return (int)type;
    }
}
