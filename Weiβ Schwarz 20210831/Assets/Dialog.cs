using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialog : MonoBehaviour
{
    [SerializeField] GameObject System;
    [SerializeField] Text text;
    public bool isYes;
    public bool isReset;
    string Event;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    Dialog()
    {
        isYes = false;
        isReset = true;
        Event = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onYesBtn()
    {
        if(Event == "clockSkip")
        {
            clockSkip();
        }
    }

    public void onNoBtn()
    {
        if (Event == "clockSkip")
        {
            DoClock();
        }
    }

    public void setEvent(string eventName)
    {
        Event = eventName;
    }

    public void setText(string guide)
    {
        text.GetComponent<Text>().text = guide;
    }

    public void clockSkip()
    {
        System.GetComponent<Game>().clockSkip();
        this.gameObject.SetActive(false);
    }

    public void DoClock()
    {
        System.GetComponent<Game>().DoClock();
        this.gameObject.SetActive(false);
    }
}
