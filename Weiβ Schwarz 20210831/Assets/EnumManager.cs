using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumManager : MonoBehaviour
{
    public enum Zone
    {
        DECK,
        HAND,
        CLOCK,
        FIELD,
        STOCK,
        MEMORY,
        LEVEL0,
        LEVEL1,
        LEVEL2,
        GRAVEYARD,
        CLIMAX,
        EVENT,
        VOID,
    }

    public enum Phase
    {
        P1_DRAW,
        P1_CLOCK,
        P1_MAIN,
        P1_CLIMAX,
        P1_ATTACK,
        P1_END,
        P2_DRAW,
        P2_CLOCK,
        P2_MAIN,
        P2_CLIMAX,
        P2_ATTACK,
        P2_END,
    }

    public enum Type
    {
        Character,
        Climax,
        Event,
        Void,
    }

    public enum Trigger
    {
        COMEBACK,
        STANDBY,
        DRAW,
        GATE,
        SHOT,
        BOUNCE,
        TREASURE,
        POOL,
        NONE,
        SOUL,
        DOUBLE_SOUL,
        VOID,
    }

    public enum Status
    {
        STAND,
        REST,
        REVERSE,
        VOID,
    }
}
