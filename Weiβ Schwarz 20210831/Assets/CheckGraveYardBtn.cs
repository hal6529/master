using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckGraveYardBtn : MonoBehaviour
{
    [SerializeField] GameObject CheckGraveYardCards;
    [SerializeField] GameObject System;

    public void onCheckGraveYardBtn()
    {
        CheckGraveYardCards.SetActive(true);
        this.gameObject.SetActive(false);
        System.GetComponent<Game>().setGraveYardScrollView();
    }
}
