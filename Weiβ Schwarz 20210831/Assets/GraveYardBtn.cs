using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraveYardBtn : MonoBehaviour
{
    [SerializeField] GameObject System;
    [SerializeField] GameObject CheckGraveYardBtn;

    public void spriteChange()
    {
        int cnt = System.GetComponent<Game>().getMyTrashListCount() - 1;
        Sprite sprite;
        if (cnt <= 0)
        {
            sprite = null;
            this.gameObject.GetComponent<Image>().color = new Color(255f / 255f, 255f / 255f, 255f / 255f, 0f / 255f);
        }
        else
        {
            sprite = System.GetComponent<Game>().getCardSprite(cnt);
            this.gameObject.GetComponent<Image>().color = new Color(255f / 255f, 255f / 255f, 255f / 255f, 255f / 255f);
        }
        GetComponent<Image>().sprite = sprite;
    }

    public void onCheckGraveYardBtn()
    {
        if(System.GetComponent<Game>().getMyTrashListCount() == 0)
        {
            return;
        }

        // PlayBtn全てをfalseにする
        System.GetComponent<CardBtnManager>().onFalseCardPlayBtn();
        // MoveBtnとEffectBtn全てをfalseにする
        System.GetComponent<MoveBtnAndEffectBtnManager>().onFalseMoveBtns();
        CheckGraveYardBtn.SetActive(true);
    }
}
