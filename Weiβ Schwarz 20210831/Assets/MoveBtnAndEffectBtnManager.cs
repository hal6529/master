using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBtnAndEffectBtnManager : MonoBehaviour
{
    [SerializeField] List<GameObject> EffectBtnList;
    [SerializeField] List<GameObject> MoveBtnList;
    [SerializeField] List<GameObject> MoveWideBtnList;

    void onFalseEffectBtn()
    {
        for (int i = 0; i < EffectBtnList.Count; i++)
        {
            EffectBtnList[i].SetActive(false);
        }
    }

    void onFalseMoveBtn()
    {
        for (int i = 0; i < MoveBtnList.Count; i++)
        {
            MoveBtnList[i].SetActive(false);
        }
    }

    void onFalseMoveWideBtn()
    {
        for (int i = 0; i < MoveWideBtnList.Count; i++)
        {
            MoveWideBtnList[i].SetActive(false);
        }
    }

    public void onFalseMoveBtns()
    {
        onFalseEffectBtn();
        onFalseMoveBtn();
        onFalseMoveWideBtn();
    }
}
