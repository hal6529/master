using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardImageGuide : MonoBehaviour
{
    public void changeImage(Sprite image)
    {
        GetComponent<Image>().sprite = image;
    }
}
