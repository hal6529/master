using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class onAttackAction : EnumManager
{
    [SerializeField] GameObject SystemObject;

    private int phase;

    public void onAttckBtn()
    {
        phase = SystemObject.GetComponent<TimeManager>().getPhase();
        if (phase != (int)Phase.P1_ATTACK || phase != (int)Phase.P2_ATTACK)
        {
            return;
        }
    }
}
