using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    [SerializeField] List<GameObject> AdventureTimeEventList = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onEventManager(string cardNo)
    {
        if (cardNo.Equals("AT_WX02_A07"))
        {
            AdventureTimeEventList[0].GetComponent<eventQuestReceived>().onQuestReceived();
        }
    }

}
