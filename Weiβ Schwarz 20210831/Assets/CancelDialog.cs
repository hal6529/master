using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CancelDialog : MonoBehaviour
{
    [SerializeField] GameObject System;
    [SerializeField] Text text;

    string Event;
    CancelDialog()
    {
        Event = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onCancelBtn()
    {
        
    }

    public void setEvent(string eventName)
    {
        Event = eventName;
    }

    public void setText(string guide)
    {
        text.GetComponent<Text>().text = guide;
    }
}
