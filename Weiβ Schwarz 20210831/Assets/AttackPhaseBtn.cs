using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPhaseBtn : MonoBehaviour
{
    [SerializeField] GameObject System;
    private int phase;

    // ここを変更する場合はGame,TimeManager,onAttackAction,CardBtn,AttackPhaseBtnも変更すること
    enum Phase
    {
        P1_DRAW,
        P1_CLOCK,
        P1_MAIN,
        P1_CLIMAX,
        P1_ATTACK,
        P1_END,
        P2_DRAW,
        P2_CLOCK,
        P2_MAIN,
        P2_CLIMAX,
        P2_ATTACK,
        P2_END,
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onAttackPhaseBtn()
    {
        phase = System.GetComponent<TimeManager>().getPhase();

        if (phase != (int)Phase.P1_MAIN && phase != (int)Phase.P2_MAIN)
        {
            return;
        }

        int attackPhase = (int)Phase.P1_ATTACK;
        System.GetComponent<TimeManager>().setPhase(attackPhase);
    } 
}
