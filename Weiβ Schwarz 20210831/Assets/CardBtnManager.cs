using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardBtnManager : MonoBehaviour
{
    [SerializeField] List<GameObject> BtnList;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onFalseCardPlayBtn()
    {
        for(int i = 0; i < BtnList.Count; i++)
        {
            BtnList[i].SetActive(false);
        }
    }
}
