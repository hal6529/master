using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardBtn : EnumManager
{
    [SerializeField] GameObject System;
    [SerializeField] GameObject CardPlayBtn;
    [SerializeField] GameObject moveWideButton;
    [SerializeField] GameObject moveButton;
    [SerializeField] GameObject effectButton;
    [SerializeField] GameObject attackButton;
    [SerializeField] GameObject CardSetObject;
    [SerializeField] GameObject CardImageGuide;

    /**
     * TimeManagerのphase
     */
    private int phase;

    public void onMainFieldClick()
    {
        if (CardSetObject.GetComponent<Card>().GetNowZone() != (int)Zone.FIELD)
        {
            return;
        }
        phase = System.GetComponent<TimeManager>().getPhase();

        // PlayBtn全てをfalseにする
        System.GetComponent<CardBtnManager>().onFalseCardPlayBtn();
        // MoveBtnとEffectBtn全てをfalseにする
        System.GetComponent<MoveBtnAndEffectBtnManager>().onFalseMoveBtns();

        if (phase == (int)Phase.P1_MAIN || phase == (int)Phase.P2_MAIN)
        {
            if (CardSetObject.GetComponent<Card>().haveActEffect)
            {
                moveButton.SetActive(true);
                effectButton.SetActive(true);
            }
            else
            {
                moveWideButton.SetActive(true);
            }
        } else if (phase == (int)Phase.P1_ATTACK || phase == (int)Phase.P2_ATTACK)
        {
            attackButton.SetActive(true);
        }
    }

    public void onMainHandClick()
    {
        if (CardSetObject.GetComponent<Card>().GetNowZone() != (int)Zone.HAND)
        {
            return;
        }
        phase = System.GetComponent<TimeManager>().getPhase();

        // PlayBtn全てをfalseにする
        System.GetComponent<UIManager>().HideUI();
        // MoveBtnとEffectBtn全てをfalseにする
        System.GetComponent<MoveBtnAndEffectBtnManager>().onFalseMoveBtns();

        if (phase != (int)Phase.P1_MAIN && phase != (int)Phase.P2_MAIN)
        {
            return;
        }

        CardPlayBtn.SetActive(true);
        System.GetComponent<Game>().FixPosition();
        Vector3 pos = this.gameObject.GetComponent<RectTransform>().localPosition;
        pos.y = -213;
        this.gameObject.GetComponent<RectTransform>().localPosition = pos;
    }

    public void onUsuallyClick()
    {
        CardImageGuide.GetComponent<CardImageGuide>().changeImage(CardSetObject.GetComponent<Card>().spriteImage);
    }
}
