using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldCardSetBtns : EnumManager
{
    [SerializeField] GameObject SystemObject;
    private GameObject SetCard;
    [SerializeField] GameObject FieldCardSetBtnsObject;

    public void setSetCard(GameObject card)
    {
        this.SetCard = card;
    }

    public void onLeftFieldSetBtn()
    {
        SetCard.GetComponent<Card>().switchPowerDisplay(true);
        SetCard.GetComponent<Card>().changeNowZone((int)Zone.FIELD);
        FieldCardSetBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().setFieldList(0, SetCard);
        SystemObject.GetComponent<Game>().removeHandList(SetCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void onCenterFieldSetBtn()
    {
        SetCard.GetComponent<Card>().switchPowerDisplay(true);
        SetCard.GetComponent<Card>().changeNowZone((int)Zone.FIELD);
        FieldCardSetBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().setFieldList(1, SetCard);
        SystemObject.GetComponent<Game>().removeHandList(SetCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void onRightFieldSetBtn()
    {
        SetCard.GetComponent<Card>().switchPowerDisplay(true);
        SetCard.GetComponent<Card>().changeNowZone((int)Zone.FIELD);
        FieldCardSetBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().setFieldList(2, SetCard);
        SystemObject.GetComponent<Game>().removeHandList(SetCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void onBackLeftFieldSetBtn()
    {
        SetCard.GetComponent<Card>().switchPowerDisplay(true);
        SetCard.GetComponent<Card>().changeNowZone((int)Zone.FIELD);
        FieldCardSetBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().setFieldList(3, SetCard);
        SystemObject.GetComponent<Game>().removeHandList(SetCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }

    public void onBackRightFieldSetBtn()
    {
        SetCard.GetComponent<Card>().switchPowerDisplay(true);
        SetCard.GetComponent<Card>().changeNowZone((int)Zone.FIELD);
        FieldCardSetBtnsObject.SetActive(false);
        SystemObject.GetComponent<Game>().setFieldList(4, SetCard);
        SystemObject.GetComponent<Game>().removeHandList(SetCard);
        SystemObject.GetComponent<Game>().FixPosition();
    }
}
