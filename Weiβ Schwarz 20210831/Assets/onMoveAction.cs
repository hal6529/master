using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class onMoveAction : MonoBehaviour
{
    [SerializeField] GameObject moveWideButton;
    [SerializeField] GameObject moveButton;
    [SerializeField] GameObject effectButton;
    [SerializeField] GameObject CardMoveBtnObject;
    [SerializeField] GameObject CardSetObject;

    public void onMove()
    {
        moveWideButton.SetActive(false);
        moveButton.SetActive(false);
        effectButton.SetActive(false);
        CardMoveBtnObject.GetComponent<CardMoveBtns>().setMoveCard(CardSetObject);
        CardMoveBtnObject.SetActive(true);
    }
}
