﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : EnumManager
{ 
    string phase;
    [SerializeField] GameObject myCards;
    [SerializeField] GameObject Dialog;
    [SerializeField] GameObject CancelDialog;
    [SerializeField] GameObject InquiryDialog;
    [SerializeField] GameObject DeckCount;
    [SerializeField] GameObject GraveYardButton;
    [SerializeField] GameObject GraveYardScrollView;
    [SerializeField] List<GameObject> preferenceCardList = new List<GameObject>();

    [SerializeField] List<GameObject> myDeckList = new List<GameObject>();
    [SerializeField] List<GameObject> myHandList = new List<GameObject>();
    [SerializeField] List<GameObject> myFieldList = new List<GameObject>();
    [SerializeField] List<GameObject> myTrashList = new List<GameObject>();
    [SerializeField] List<GameObject> myMemoryList = new List<GameObject>();
    [SerializeField] List<GameObject> myStockList = new List<GameObject>();

    [SerializeField] GameObject climax = null;

    Dictionary<string, GameObject> cardLink = new Dictionary<string, GameObject>();

    // クロックカードの選択中画面かどうかの判定
    bool isConfirmClockCard;

    private const double DRAW_ANIMATION = 50f; 
    // Start is called before the first frame update
    void Start()
    {
        DictionarySet();
        for (int i = 0; i < 50; i++)
        {
            CardSet(myDeckList[i], GetComponent<DeckInfoDummy>().cards[i]);
            myDeckList[i].GetComponent<Card>().spriteChange();
        }
        Shuffle();
        for (int i = 0; i < 5; i++)
        {
            Draw();
        }

        GetComponent<TimeManager>().setPhase((int)Phase.P1_DRAW);
    }


    // Update is called once per frame
    void Update()
    {
        changeDeckCount();
    }

    // Dictionaryに連携するメソッド
    void DictionarySet()
    {
        cardLink = new Dictionary<string, GameObject>()
        {
            {"AT_WX02_A01", preferenceCardList[0]},
            {"AT_WX02_A02", preferenceCardList[1]},
            {"AT_WX02_A03", preferenceCardList[2]},
            {"AT_WX02_A04", preferenceCardList[3]},
            {"AT_WX02_A05", preferenceCardList[4]},
            {"AT_WX02_A06", preferenceCardList[5]},
            {"AT_WX02_A07", preferenceCardList[6]},
            {"AT_WX02_A08", preferenceCardList[7]},
            {"AT_WX02_A09", preferenceCardList[8]},
            {"AT_WX02_A10", preferenceCardList[9]},
            {"AT_WX02_A11", preferenceCardList[10]},
            {"AT_WX02_A12", preferenceCardList[11]},
            {"AT_WX02_A13", preferenceCardList[12]}
        };
    }

    // 各カードのCardのなかに情報をセットするメソッド
    void CardSet(GameObject cloneCard, string ID)
    {
        Card m_object = cardLink[ID].GetComponent<Card>();
        cloneCard.GetComponent<Card>().level = m_object.level;
        cloneCard.GetComponent<Card>().cost = m_object.cost;
        cloneCard.GetComponent<Card>().trigger = m_object.trigger;
        cloneCard.GetComponent<Card>().originalPower = m_object.originalPower;
        cloneCard.GetComponent<Card>().cardNo = m_object.cardNo;
        cloneCard.GetComponent<Card>().name = m_object.name;
        cloneCard.GetComponent<Card>().soul = m_object.soul;
        cloneCard.GetComponent<Card>().character = m_object.character;
        cloneCard.GetComponent<Card>().color = m_object.color;
        cloneCard.GetComponent<Card>().SetType(m_object.GetType());
        cloneCard.GetComponent<Card>().spriteImage = m_object.spriteImage;
        cloneCard.GetComponent<Card>().changeNowZone((int)Zone.DECK);
        cloneCard.GetComponent<Card>().haveActEffect = m_object.haveActEffect;
    }

    public void Draw()
    {
        GameObject drawCard = myDeckList[myDeckList.Count - 1];
        myHandList.Add(myDeckList[myDeckList.Count - 1]);
        myHandList[myHandList.Count - 1].GetComponent<Card>().changeNowZone((int)Zone.HAND);
        myDeckList.RemoveAt(myDeckList.Count - 1);

        StartCoroutine(Delay(1.0f, () =>
        {
            FixPosition();
        }));
    }

    public void FixPosition()
    {
        GraveYardButton.GetComponent<GraveYardBtn>().spriteChange();
        for (int i = 0; i < myDeckList.Count; i++)
        {
            RectTransform m_myDeckList = myDeckList[i].GetComponent<RectTransform>();
            m_myDeckList.localPosition = new Vector3(171, -66.277f, 0);
            m_myDeckList.SetSiblingIndex(i);
            m_myDeckList.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        for(int i = 0; i < myHandList.Count; i++)
        {
            RectTransform m_myHandCard = myHandList[i].GetComponent<RectTransform>();
            m_myHandCard.localPosition = new Vector3(-144 + 20 * i, -233, 50 - i);
            m_myHandCard.SetSiblingIndex(i);
            m_myHandCard.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        for (int i = 0; i < myFieldList.Count; i++)
        {
            if(myFieldList[i] == null)
            {
                continue;
            }
            RectTransform m_myFieldCard = myFieldList[i].GetComponent<RectTransform>();
            if(i < 3)
            {
                m_myFieldCard.localPosition = new Vector3(-44.2f + 72.9f * i, -11.9f, 0);
            }
            else
            {
                m_myFieldCard.localPosition = new Vector3(-7.2f + 72.9f * (i - 3), -82.6f, 0);
            }
            
            m_myFieldCard.SetSiblingIndex(i);
            m_myFieldCard.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        for (int i = 0; i < myTrashList.Count; i++)
        {
            RectTransform m_myTrashCard = myTrashList[i].GetComponent<RectTransform>();
            m_myTrashCard.localPosition = new Vector3(171, -135.5f, 0);
            m_myTrashCard.SetSiblingIndex(i);
            m_myTrashCard.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if(climax != null)
        {
            RectTransform m_climax = climax.GetComponent<RectTransform>();
            m_climax.localPosition = new Vector3(-85, -66, 0);
            m_climax.transform.eulerAngles = new Vector3(0, 0, 90);
        }

    }

    public void clockSkip()
    {
        if (GetComponent<TimeManager>().getPhase() != (int)Phase.P1_CLOCK || GetComponent<TimeManager>().getPhase() != (int)Phase.P2_CLOCK)
        {
            return;
        }
        GetComponent<TimeManager>().setPhase((int)Phase.P1_CLOCK);
    }

    public void DoClock()
    {
        if (GetComponent<TimeManager>().getPhase() != (int)Phase.P1_CLOCK || GetComponent<TimeManager>().getPhase() != (int)Phase.P2_CLOCK)
        {
            return;
        }
        CancelDialog m_cancelDialog = CancelDialog.GetComponent<CancelDialog>();
        m_cancelDialog.setEvent("Confirmation of the clock card");
        m_cancelDialog.setText("Please select clock card");
        CancelDialog.SetActive(true);
    }

    public void trashCard()
    {
        if(climax == null)
        {
            return;
        }
        myTrashList.Add(climax);
        climax = null;
        FixPosition();
    }

    private void Shuffle()
    {
        int n = myDeckList.Count;

        while(n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);

            // k番目のカードをtempに代入
            GameObject temp = myDeckList[k];
            myDeckList[k] = myDeckList[n];
            myDeckList[n] = temp;
        }
    }

    public void onConfirmPlayThisCard(GameObject card)
    {
        GetComponent<CardBtnManager>().onFalseCardPlayBtn();
        InquiryDialog m_InquiryDialog = InquiryDialog.GetComponent<InquiryDialog>();
        m_InquiryDialog.setText("Do you wanna play this Card??");
        m_InquiryDialog.setPlayCard(card);
        InquiryDialog.SetActive(true);
    }

    // Delayメソッド
    /* waitTimeの秒数後にactionする*/

    private IEnumerator Delay(float waitTime, Action action)
    {
        yield return new WaitForSeconds(waitTime);
        action();
    }

    public void setFieldList(int listNum, GameObject setCard)
    {
        myFieldList[listNum] = setCard;
    }

    public void removeHandList(GameObject Card)
    {
        myHandList.Remove(Card);
    }

    public void changeFieldList(int one, GameObject ExchangeCard)
    {
        int num = myFieldList.IndexOf(ExchangeCard);

        if (one >= 5 || num >= 5)
        {
            return;
        }
        
        GameObject temp = myFieldList[one];
        myFieldList[one] = myFieldList[num];
        myFieldList[num] = temp;
    }

    public void changeDeckCount()
    {
        DeckCount.GetComponent<Text>().text = myDeckList.Count.ToString();
    }

    public void setClimax(GameObject cardObject)
    {
        climax = cardObject;
    }

    public int getMyTrashListCount()
    {
        return myTrashList.Count;
    }

    public Sprite getCardSprite(int num)
    {
        return myTrashList[num].GetComponent<Card>().spriteImage;
    }

    public List<GameObject> getMyTrashList()
    {
        return myTrashList;
    }

    public void setGraveYardScrollView()
    {
        GraveYardScrollView.GetComponent<CheckGraveYardScrollView>().changeBtnSprite(myTrashList.Count);
    }

}
