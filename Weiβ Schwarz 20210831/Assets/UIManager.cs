using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject CheckGraveYardCards;

    public void HideUI()
    {
        // PlayBtn全てをfalseにする
        GetComponent<CardBtnManager>().onFalseCardPlayBtn();
        // MoveBtnとEffectBtn全てをfalseにする
        GetComponent<MoveBtnAndEffectBtnManager>().onFalseMoveBtns();
        CheckGraveYardCards.SetActive(false);
    }
}
