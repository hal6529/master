using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckGraveYardScrollView : MonoBehaviour
{
    [SerializeField] List<GameObject> cardBtnList = new List<GameObject>();
    private List<GameObject> myTrashList = new List<GameObject>();
    [SerializeField] GameObject SystemObject;
    
    public void changeBtnSprite(int num)
    {
        setMyTrashList();
        for(int i = 0 ; i < cardBtnList.Count ; i++)
        {
            cardBtnList[i].SetActive(false);
        }

        for (int i = 0 ; i < num ; i++)
        {
            cardBtnList[i].SetActive(true);
            cardBtnList[i].GetComponent<Image>().sprite = myTrashList[i].GetComponent<Card>().spriteImage;
        }       
    }

    void setMyTrashList()
    {
        this.myTrashList = SystemObject.GetComponent<Game>().getMyTrashList();
    }
}
