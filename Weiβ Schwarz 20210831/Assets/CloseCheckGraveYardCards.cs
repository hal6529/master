using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseCheckGraveYardCards : MonoBehaviour
{
    [SerializeField] GameObject CheckGraveYardCards;

    public void onCloseCheckGraveYardCards()
    {
        CheckGraveYardCards.SetActive(false);
    }
}
